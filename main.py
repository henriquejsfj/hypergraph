import Hipergrafo as grafo
from os import listdir
def parApar(n: list, G):
    if len(n)<=2:
        G.arestaAdd(int(n[0]),int(n[1]))
        return n
    else:
        for e in (n[1:]):
            parApar([n[0],e],G)
    parApar(n[1:],G)
#rel = open("RELATORIO3.txt",'w')
for i in listdir():
    if ".matriz" in i:
        arq = i
        linhas = open(arq).readlines()
        H = grafo.Hipergrafo(Numvertices=int(linhas[0]))
        G = grafo.Hipergrafo(Numvertices=int(linhas[0]))
        for linha in linhas[1:]:
            lista = [int(i) for i in linha.split(';')]
            H.arestaAdd(*lista)
            parApar(lista, G)
        if G.verticesGet()==H.verticesGet():
            vertices = G.verticesGet()
        else:
            print("HEEEEYYYY")
            break
        #v = vertices.pop()
        explorados = set()
        
        arq2 = open(arq.split('.')[0]+'H2.csv','w')
        arq3 = open(arq.split('.')[0]+'G2.csv','w')
        vorder = list(vertices)
        vorder.sort()
        x = ';'+str(vorder).replace(",",";").replace("[",'').replace("]",'')+'\n'
        arq2.write(x)
        arq3.write(x)

        for v in vorder:
            arq2.write('%d;'%v +(v-1)*';')
            arq3.write('%d;'%v +(v-1)*';')
            #lH = []
            #lG = []
            for u in sorted(vertices-explorados):
                #lH.append(H.cosine_similarity(v,u))
                #lG.append(G.cosine_similarity(v,u))
                HC = H.mine_similarity2(v,u)
                #if HC>=1: print(HC)
                arq2.write(('%f;'%HC).replace('.',','))
                arq3.write(('%f;'%G.mine_similarity2(v,u)).replace('.',','))
            arq2.write('\n')
            arq3.write('\n')
            explorados.add(v)
        arq2.close()
        arq3.close()
        H.Relatorio(arquivo = arq.split('.')[0]+" - Relatorio.txt")
        '''
        maximoH=max(lH)
        maximoG=max(lG)
        if lH.index(maximoH)!=lG.index(maximoG) or lH.count(maximoH)!=lG.count(maximoG):
        #if maximoH!=maximoG:
            try:
                outro = open(arq.split('.')[0]+'.idsgrafo', encoding="UTF-8")
                dcod = {}
                for cod in outro.readlines():
                    lcod = cod.split(':')
                    dcod[int(lcod[1])] = lcod[0]
            except Exception as erro:
                print(arq, erro)
                continue
            print(30*'_', file=rel)
            print(arq,'-',G.num_verticesGet(), file=rel)
            print(maximoH, file=rel)
            print("Teve:",lH.count(maximoH),"Exemplo:",lH.index(maximoH)+2, dcod[lH.index(maximoH)+2], file=rel)
            print(30*'..', file=rel)
            print(maximoG, file=rel)
            print("Teve:",lG.count(maximoG),"Exemplo:",lG.index(maximoG)+2, dcod[lG.index(maximoG)+2], file=rel)
            print(30*'_', file=rel)'''
#rel.close()
