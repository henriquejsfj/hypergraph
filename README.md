# Hypergraph



This repository it has codes to represent Graph in Matrix or Adjacency Vector. Also it can represent Hypergraph making a vector which index denotes nodes and the elements on that index are Edges.

After the codes to manipulate Hypergraph could become a separated repository just to do it, the directory graph_module it already has the codes for Graph in Python Module style.

The focus on this repository is the analyses containing on the article Hypergraph.pdf, the datasets are on dataset directory.

The codes should run inside the dataset directory, the main.py do the analyses.

After I will document better this repository.
