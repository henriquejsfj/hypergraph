﻿from array import array
class Fila():
    def __init__(self, iteravel=[]):    self.__fila=list(iteravel)
    def add(self,e):    self.__fila.append(e)
    def get(self):  return self.__fila.pop(0)
    def __eq__(self,o): return self.__fila == o
class Pilha():
    def __init__(self, iteravel=[]):    self.__pilha=list(iteravel)
    def add(self,e):    self.__pilha.insert(0,e)
    def get(self):  return self.__pilha.pop(0)
    def __eq__(self,o): return self.__pilha == o
        
class Grafo():
    '''Cria um grafo com 2 opções de representação: VetorAdj ou MatrizAdj.

Inicializa o grafo a partir de um arquivo. Não é recomendável criar um grafo usando essa função, utilize VetorAdj ou MatrizAdj para definir a sua representação.'''
    def __init__(self, arquivo=None, sep=' ', Numvertices=0):
        self.__arquivo=arquivo
        self.__vertices = set()
        if arquivo:
            linhas = open(arquivo).readlines() #lista com as linhas do arquivo
            self.__num_vertices = int(linhas[0]) #acessa a primeira linha do arquivo
            self.__num_arestas = len(linhas[1:]) #tamanho da lista com todas as linhas menos a primeira
            self.__grauVetor = list(0 for i in range(self.__num_vertices)) #criei lista de graus para acessar rapido
            for linha in linhas[1:]:
                vlinha = linha.split(sep) #divide cada linha no caracter <espaço>
                self.arestaAdd(*vlinha)
        elif Numvertices:
            self.__linhas = None
            self.__num_vertices = Numvertices
            self.__num_arestas = 0
            self.__grauVetor = list(0 for i in range(self.__num_vertices))
        else:
            self.__linhas = None
            self.__num_vertices = 0
            self.__num_arestas = 0
            self.__grauVetor = None
            
    def verticeAdd(self, v: int):
        self.__vertices.add(v)
    def grauVetorSet(self, *args):
        for v in args:
            self.__grauVetor[v-1] += 1
            self.verticeAdd(v)
    def grauVetorGet(self):
        return self.__grauVetor
    def grauVertice(self, v: int):
        return self.__grauVetor[v-1]
    
    def distEmpirica_grau(self):
        dic={}
        for grau in range(min(self.__grauVetor),max(self.__grauVetor)+1):
            div = self.__grauVetor.count(grau)/self.__num_vertices
            if div!=0:
                dic[grau] = float("%.2f" %div)
            else:
                dic[grau] = int(div)
        return dic

    def verticesGet(self):
        return self.__vertices.copy()
    def num_verticesGet(self):
        return self.__num_vertices
    def num_arestasGet(self):
        return self.__num_arestas

    def arquivoGet(self):
        return self.__arquivo

    def adar_adamic_similarity(self, v1, v2):
        from math import log10
        viz_v1 = set(self.vizinhos(v1))
        viz_v2 = set(self.vizinhos(v2))
        viz_comum = viz_v1&viz_v2
        soma = 0
        for v in viz_comum:
            soma+= 1/(log10(self.grauVertice(v))+1) #somei 1 pra os casos em que o grau for 1
        return soma
    def jaccard_similarity(self, v1, v2):
        viz_v1 = set(self.vizinhos(v1))
        viz_v2 = set(self.vizinhos(v2))
        viz_comum = viz_v1&viz_v2
        viz_uniao = viz_v1|viz_v2
        return len(viz_comum)/len(viz_uniao)
    def cosine_similarity(self, v1, v2):
        viz_v1 = set(self.vizinhos(v1))
        viz_v2 = set(self.vizinhos(v2))
        viz_comum = viz_v1&viz_v2
        return len(viz_comum)/((self.grauVertice(v1)**0.5)*(self.grauVertice(v2)**0.5))
    def my_similarity(self, *args):
        from math import log2
        viz_comum = set(self.vizinhos(args[0]))
        for v in args:
            viz_comum = set(self.vizinhos(v))&viz_comum
        if len(viz_comum)==0:
            return 0
        part = 0
        pesos_part = 0
        for v in args:
            soma = 0
            pesos = 0
            for aresta in self.vizinhos_arestas(v):
                inter = set(aresta)&viz_comum
                Tinter = len(inter)
                if Tinter>0:
                    Sgrau = 0
                    for h in inter:
                        Sgrau+=self.grauVertice(h)
                    grauM = Sgrau/Tinter
                    ref = (log2(len(aresta)-1)+1)/(log2(grauM)+1)
                    soma += ref * (Tinter/(len(aresta)-1))
                    pesos += ref
            Ppart = 1/self.grauVertice(v)
            part += Ppart * (soma/pesos)
            pesos_part += Ppart
        return (part/pesos_part)
    def my_similarity2(self, *args):
        from math import log2
        viz_comum = set(self.vizinhos(args[0]))
        for v in args:
            viz_comum = set(self.vizinhos(v))&viz_comum
        if len(viz_comum)==0:
            return 0
        part = 0
        pesos_part = 0
        for v in args:
            soma = 0
            pesos = 0
            for aresta in self.vizinhos_arestas(v):
                inter = set(aresta)&viz_comum
                Tinter = len(inter)
                if Tinter>0:
                    Sgrau = 0
                    for h in inter:
                        Sgrau+=self.grauVertice(h)
                    grauM = Sgrau/Tinter
                else:
                    grauM = sum(aresta)/len(aresta)
                ref = (log2(len(aresta)-1)+1)/(log2(grauM)+1)
                soma += ref * (Tinter/(len(aresta)-1))
                pesos += ref
            Ppart = 1
            part += Ppart * (soma/pesos)
            pesos_part += Ppart
        return (part/pesos_part)
            
    def BFS(self,s: int, final=None):
        '''Realiza a Breadth First Search a partir de s.

Retorna duas listas, uma com o vetor de pai e outra com o vetor dos níveis.'''
        VetorM = list(None for h in range(max(self.verticesGet())+1))
        for i in self.verticesGet():
            VetorM[i]=False
        Pai = VetorM[:]
        Nivel = VetorM[:]
        fila = Fila()
        Nivel[s] = 0
        VetorM[s] = True
        fila.add(s)
        while fila!=[]:
            u = fila.get()
            if u == final:
                return Pai,Nivel
            for v in self.vizinhos(u):
                if not(VetorM[v]):
                    Pai[v]=u
                    Nivel[v]=Nivel[u]+1
                    VetorM[v]=True
                    fila.add(v)
        '''if conexos:
            conj=set()
            for e in range(1,VetorM):
                if VetorM[e]:
                    lista.add(e)
            self.BFS(min(self.verticesGet()-conj),conexos=True)
            yield conj
        else:
            '''
        return Pai,Nivel
    def DFS(self,s,final=None):
        '''Realiza a Depth First Search a partir de s.'''
        VetorM = list(None for h in range(max(self.verticesGet())+1))
        for i in self.verticesGet():
            VetorM[i]=False
        Pai = VetorM[:]
        Nivel = VetorM[:]
        pilha = Pilha()
        Nivel[s] = 0
        pilha.add(s)
        while pilha!=[]:
            u = pilha.get()
            if u == final:
                return Pai,Nivel
            if not(VetorM[u]):
                VetorM[u] = True
                for v in self.vizinhos(u):
                        Pai[v]=u
                        Nivel[v]=Nivel[u]+1
                        pilha.add(v)
        return Pai,Nivel
    def componentes_conexas(self) ->list:
        '''Retorna uma lista com as componentes conexas da maior componente pra menor.'''
        conj=set()
        componentes = []
        while self.verticesGet()!=conj:
            s = min(self.verticesGet()-conj)
            VetorM = list(None for h in range(max(self.verticesGet())+1))
            for i in self.verticesGet():
                VetorM[i]=False
            fila = Fila()
            VetorM[s] = True
            fila.add(s)
            conj.add(s)
            while fila!=[]:
                u = fila.get()
                for v in self.vizinhos(u):
                    if not(VetorM[v]):
                        VetorM[v]=True
                        fila.add(v)
                        conj.add(v)
            componentes.append(conj)
        
        componentes.sort(key=len, reverse=True)
        path = self.arquivoGet().replace('\\','!@#$%^&*\\').split('\\')
        arquivo = open(''.join(path[:-1]).replace('!@#$%^&*','\\')+"Conexas-"+path[-1],'w') #cria arquivo
        arquivo.write("Número de componentes conexas: %d\n" %len(componentes))
        for c in range(len(componentes)):
            arquivo.write("Componente %d tem %d vértices: %s\n" %(c,len(componentes[c]),componentes[c]))
        arquivo.close()
        return componentes

    def Relatorio(self, arquivo=None):
        '''Gera um Relatório das principais características do grafo no mesmo formato e diretório do arquivo gerador'''
        if not(arquivo): arquivo = self.arquivoGet()
        path = arquivo.replace('\\','!@#$%^&*\\').split('\\')
        relatorio = open(''.join(path[:-1]).replace('!@#$%^&*','\\')+"Relatorio-"+path[-1],'w') #cria arquivo
        relatorio.write(
'''# n = %d
# m = %d
# d_medio = %.1f
'''     %(self.num_verticesGet(),self.num_arestasGet(),sum(self.grauVetorGet())/self.num_verticesGet())) #escreve no arquivo
        relatorio.write(str(self.distEmpirica_grau()).replace('{','').replace('}','').replace(':','').replace(', ','\n'))   

        relatorio.close()
    
    ######################################################################################################################
    
class VetorAdj(Grafo):
    '''Um grafo com representação em Vetor de adjacência'''
    def __init__(self,arquivo=None, sep=' ', Numvertices=0):
        '''Inicializa a lista de adjacência a partir de um arquivo'''
        super().__init__(arquivo, sep, Numvertices)
        self.__listaadj = list([] for i in range(self.num_verticesGet())) #criei a lista de adj (vetor de listas)
                
    def arestaAdd(self, v1: int, v2: int):
        self.__listaadj[v2-1].append(v1)
        self.__listaadj[v1-1].append(v2)
        self.grauVetorSet(v1,v2)
    def listaadjGet(self):
        return self.__listaadj
    def vizinhos(self,v):
        return self.__listaadj[v-1]
    
    ######################################################################################################################
    
class MatrizAdj(Grafo):
    '''Um grafo com representação em Matriz de adjacência'''
    def __init__(self, arquivo = None, sep=' ', Numvertices=0):
        '''Inicializa a matriz de adjacência a partir de um arquivo'''
        super().__init__(arquivo, sep, Numvertices)
        self.__matrizadj=[array('b', [0 for i in range(self.num_verticesGet())]) for h in range(self.num_verticesGet())] #criei a matriz de adj (vetor de array)
            
    def arestaAdd(self, v1: int, v2: int):
        self.__matrizadj[v1-1][v2-1] = 1
        self.__matrizadj[v2-1][v1-1] = 1
        self.grauVetorSet(v1,v2)
    def matrizadjGet(self):
        return self.__matrizadj
    def vizinhos(self, v):
        c=0
        for e in range(self.num_verticesGet()):
            if self.__matrizadj[v-1][e] == 1:
                c+=1
                yield e
                if c==self.grauVertice(v):
                    break
    
    ######################################################################################################################
    
class Hipergrafo(Grafo):
    '''Cria um Hipergrafo com representação em vetor de arestas.'''
    def __init__(self, arquivo=None, sep=' ', Numvertices=0):
        '''Inicializa a lista de adjacência a partir de um arquivo'''
        super().__init__(arquivo, sep, Numvertices)
        self.__arestas = []
        self.__hiper = list([] for i in range(self.num_verticesGet())) #criei a lista de adj (vetor de listas)
        #self.grauVetorSet(v1,v2)
    def arestasGet(self, *args):
        return self.__arestas
    def arestaAdd(self, *args):
        E = array('H', args)
        self.__arestas.append(E)
        for v in args:
            self.__hiper[v-1].append(E)
        self.grauVetorSet(*args)
    def hiperGet(self):
        return self.__hiper
    def vizinhos_arestas(self, v):
        return self.__hiper[v-1]
    def vizinhos(self,v):
        for a in self.__hiper[v-1]:
            for s in a:
                if s!=v: yield s
    
    ######################################################################################################################
                
def relatorio_arvore(pai,nivel,arquivo="arvore.txt"):
    arq=open(arquivo,'w')
    for p in range(1,len(pai)):
        arq.write("Vértice: %d filho de %d no nível %d \n"%(p,pai[p],nivel[p]))
    arq.close()

