﻿# Autor: Henrique José dos Santos Ferreira Júnior
# Contato: henriqueferreira@ufrj.br

'''Interface do gráfico sem a representação estrutural'''

__all__ = ['Grafo']

from grafo._pilha import Pilha
from grafo._fila import Fila

class Grafo():
    '''Cria um grafo com 2 opções de representação: VetorAdj ou MatrizAdj.

Inicializa o grafo a partir de um arquivo. Não é recomendável criar um grafo usando essa função, utilize VetorAdj ou MatrizAdj para definir a sua representação.'''
    def __init__(self, arquivo=None):
        self.__arquivo=arquivo
        self.__vertices = set()
        if arquivo:
            linhas = open(arquivo).readlines() #lista com as linhas do arquivo
            self.__linhas = linhas
            self.__num_vertices = int(linhas[0]) #acessa a primeira linha do arquivo
            self.__num_arestas = len(linhas[1:]) #tamanho da lista com todas as linhas menos a primeira
            self.__grauVetor = list(0 for i in range(self.__num_vertices)) #criei lista de graus para acessar rapido
        else:
            self.__linhas = None
            self.__num_vertices = 0
            self.__num_arestas = 0
            self.__grauVetor = None
            
    def verticeAdd(self, v: int):
        self.__vertices.add(v)
    def grauVetorSet(self, v1: int, v2: int):
        self.__grauVetor[v2-1] += 1
        self.__grauVetor[v1-1] += 1
        self.verticeAdd(v1)
        self.verticeAdd(v2)
    def grauVetorGet(self):
        return self.__grauVetor
    def grauVertice(self, v: int):
        return self.__grauVetor[v-1]
    
    def distEmpirica_grau(self):
        dic={}
        for grau in range(min(self.__grauVetor),max(self.__grauVetor)+1):
            div = self.__grauVetor.count(grau)/self.__num_vertices
            if div!=0:
                dic[grau] = float("%.2f" %div)
            else:
                dic[grau] = int(div)
        return dic

    def verticesGet(self):
        return self.__vertices
    def num_verticesGet(self):
        return self.__num_vertices
    def num_arestasGet(self):
        return self.__num_arestas

    def arquivoGet(self):
        return self.__arquivo
    def linhasGet(self):
        return self.__linhas
    
    def BFS(self,s: int, final=None):
        '''Realiza a Breadth First Search a partir de s.

Retorna duas listas, uma com o vetor de pai e outra com o vetor dos níveis.'''
        VetorM = list(None for h in range(max(self.verticesGet())+1))
        for i in self.verticesGet():
            VetorM[i]=False
        Pai = VetorM[:]
        Nivel = VetorM[:]
        fila = Fila()
        Nivel[s] = 0
        VetorM[s] = True
        fila.add(s)
        while fila!=[]:
            u = fila.get()
            if u == final:
                return Pai,Nivel
            for v in self.vizinhos(u):
                if not(VetorM[v]):
                    Pai[v]=u
                    Nivel[v]=Nivel[u]+1
                    VetorM[v]=True
                    fila.add(v)
        '''if conexos:
            conj=set()
            for e in range(1,VetorM):
                if VetorM[e]:
                    lista.add(e)
            self.BFS(min(self.verticesGet()-conj),conexos=True)
            yield conj
        else:
            '''
        return Pai,Nivel
    def DFS(self,s,final=None):
        '''Realiza a Depth First Search a partir de s.'''
        VetorM = list(None for h in range(max(self.verticesGet())+1))
        for i in self.verticesGet():
            VetorM[i]=False
        Pai = VetorM[:]
        Nivel = VetorM[:]
        pilha = Pilha()
        Nivel[s] = 0
        pilha.add(s)
        while pilha!=[]:
            u = pilha.get()
            if u == final:
                return Pai,Nivel
            if not(VetorM[u]):
                VetorM[u] = True
                for v in self.vizinhos(u):
                        Pai[v]=u
                        Nivel[v]=Nivel[u]+1
                        pilha.add(v)
        return Pai,Nivel
    def componentes_conexas(self) ->list:
        '''Retorna uma lista com as componentes conexas da maior componente pra menor.'''
        conj=set()
        componentes = []
        while self.verticesGet()!=conj:
            s = min(self.verticesGet()-conj)
            VetorM = list(None for h in range(max(self.verticesGet())+1))
            for i in self.verticesGet():
                VetorM[i]=False
            fila = Fila()
            VetorM[s] = True
            fila.add(s)
            conj.add(s)
            while fila!=[]:
                u = fila.get()
                for v in self.vizinhos(u):
                    if not(VetorM[v]):
                        VetorM[v]=True
                        fila.add(v)
                        conj.add(v)
            componentes.append(conj)
        
        componentes.sort(key=len, reverse=True)
        path = self.arquivoGet().replace('\\','!@#$%^&*\\').split('\\')
        arquivo = open(''.join(path[:-1]).replace('!@#$%^&*','\\')+"Conexas-"+path[-1],'w') #cria arquivo
        arquivo.write("Número de componentes conexas: %d\n" %len(componentes))
        for c in range(len(componentes)):
            arquivo.write("Componente %d tem %d vértices: %s\n" %(c,len(componentes[c]),componentes[c]))
        arquivo.close()
        return componentes

    def Relatorio(self):
        '''Gera um Relatório das principais características do grafo no mesmo formato e diretório do arquivo gerador'''
        path = self.arquivoGet().replace('\\','!@#$%^&*\\').split('\\')
        relatorio = open(''.join(path[:-1]).replace('!@#$%^&*','\\')+"Relatorio-"+path[-1],'w') #cria arquivo
        relatorio.write(
'''# n = %d
# m = %d
# d_medio = %.1f
'''     %(self.num_verticesGet(),self.num_arestasGet(),sum(self.grauVetorGet())/self.num_verticesGet())) #escreve no arquivo
        relatorio.write(str(self.distEmpirica_grau()).replace('{','').replace('}','').replace(':','').replace(', ','\n'))   

        relatorio.close()