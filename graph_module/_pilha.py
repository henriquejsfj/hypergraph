# Autor: Henrique José dos Santos Ferreira Júnior
# Contato: henriqueferreira@ufrj.br

'''Pilha básica para execução da BFS.'''

__all__ = ['Pilha']

class Pilha():
    def __init__(self, iteravel=[]):    self.__pilha=list(iteravel)
    def add(self,e):    self.__pilha.insert(0,e)
    def get(self):  return self.__pilha.pop(0)
    def __eq__(self,o): return self.__pilha == o
