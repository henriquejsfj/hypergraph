﻿# Autor: Henrique José dos Santos Ferreira Júnior
# Contato: henriqueferreira@ufrj.br

'''Implementa a interface do grafo com a representação em Vetor de adjacência'''

__all__ = ['VetorAdj']

from grafo._grafo import Grafo

class VetorAdj(Grafo):
    '''Um grafo com representação em Vetor de adjacência'''
    def __init__(self,arquivo=None):
        '''Inicializa a lista de adjacência a partir de um arquivo'''
        super().__init__(arquivo)
        self.__listaadj = list([] for i in range(self.num_verticesGet())) #criei a lista de adj (vetor de listas)
        if self.arquivoGet():
            for linha in self.linhasGet()[1:]:
                vlinha = linha.split(' ') #divide cada linha no caracter <espaço>
                v1, v2 = int(vlinha[0]), int(vlinha[1])
                self.__listaadj[v2-1].append(v1)
                self.__listaadj[v1-1].append(v2)
                self.grauVetorSet(v1,v2)
                
    def arestaAdd(self, v1: int, v2: int):
        self.__listaadj[v2-1].append(v1)
        self.__listaadj[v1-1].append(v2)
        self.grauVetorSet(v1,v2)
    def listaadjGet(self):
        return self.__listaadj
    def vizinhos(self,v):
        return self.__listaadj[v-1]
