# Autor: Henrique José dos Santos Ferreira Júnior
# Contato: henriqueferreira@ufrj.br

'''Fila Básica para execução da BFS.'''

__all__ = ['Fila']

class Fila():
    def __init__(self, iteravel=[]):    self.__fila=list(iteravel)
    def add(self,e):    self.__fila.append(e)
    def get(self):  return self.__fila.pop(0)
    def __eq__(self,o): return self.__fila == o
