﻿# Autor: Henrique José dos Santos Ferreira Júnior
# Contato: henriqueferreira@ufrj.br

'''Implementa a interface do grafo com a representação em Matriz de adjacência'''

__all__ = ['MatrizAdj']

from grafo._grafo import Grafo
from array import array

class MatrizAdj(Grafo):
    '''Um grafo com representação em Matriz de adjacência'''
    def __init__(self, arquivo = None):
        '''Inicializa a matriz de adjacência a partir de um arquivo'''
        super().__init__(arquivo)
        self.__matrizadj=[array('b', [0 for i in range(self.num_verticesGet())]) for h in range(self.num_verticesGet())] #criei a matriz de adj (vetor de array)
        for linha in self.linhasGet()[1:]:
            vlinha = linha.split(' ') #divide cada linha no caracter <espaço>
            v1, v2 = int(vlinha[0]), int(vlinha[1])
            self.__matrizadj[v1-1][v2-1] = 1
            self.__matrizadj[v2-1][v1-1] = 1
            self.grauVetorSet(v1,v2)
            
    def arestaAdd(self, v1: int, v2: int):
        self.__matrizadj[v1-1][v2-1] = 1
        self.__matrizadj[v2-1][v1-1] = 1
        self.grauVetorSet(v1,v2)
    def vizinhos(self, v):
        c=0
        for e in range(len(self.__matrizadj[v-1])):
            if self.__matrizadj[v-1][e] == 1:
                c+=1
                yield e
                if c==self.grauVertice(v):
                    break
