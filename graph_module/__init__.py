﻿# Autor: Henrique José dos Santos Ferreira Júnior
# Contato: henriqueferreira@ufrj.br

'''Um pacote para criação e manipulação de grafos.'''

__version__ = '0.1.0'

__all__ = ['relatorio_arvore']

def relatorio_arvore(pai,nivel,arquivo="arvore.txt"):
    '''Gera relatório da árvore induzida pela BFS ou DFS.'''
    arq=open(arquivo,'w')
    for p in range(1,len(pai)):
        arq.write("Vértice: %d filho de %d no nível %d \n"%(p,pai[p],nivel[p]))
    arq.close()
